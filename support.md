# Known issues

* **2013-06-27** x86_64 Linux TBB extraction error ([#6597](https://bugs.torproject.org/6597))
* **2013-05-22** ExitNodes 1 being ignored ([#8934](https://bugs.torproject.org/8934))
* **2013-05-19** Pluggable transports bundles warn of need to upgrade when no new version is yet available ([#8645](https://bugs.torproject.org/8645))
* **2013-04-17** TBB with Firefox 17 ESR is exhibiting slow, clunky UI performance ([#8352](https://bugs.torproject.org/8352))
* **2013-04-12** Gettor: TBB too big for Gmail ([#8542](https://bugs.torproject.org/8542))
* **2013-06-01** Turkish Tor Browser Bundle ([9010](https://bugs.torproject.org/9010))
* **2014-01-07** Tor Browser's Private Browsing Mode breaks sites ([#10569](https://bugs.torproject.org/10569))
* **2014-03-27**  Tor Launcher configuration wizard has no text in Korean and Vietnamese ([#11336](https://bugs.torproject.org/11336))

## Affecting Firefox 24 based bundles

* **2013-04-20** Lack of retina display support due to FF version ([#8750](https://bugs.torproject.org/8750))

# Frequent questions

* **2013-04-17** Why Ghostery / Adblock+ will not be added to the TBB ([#4188](https://bugs.torproject.org/4188))
* **2014-01-03**  torbrowser in the apple app store is fake ([10549](https://bugs.torproject.org/10549))

To edit this page, clone the git repo at https://git.torproject.org/project/help/wiki.git and ask Lunar to merge your changes.
