# Design

## Access control

Multiple people have access to the SVN server, in order:

## Layer 0: "the feds"

While the virtual machine is (now) hosted on a server with full disk
encryption, it's technically possible that a hostile party with physical
access to the machine (or a 0-day) would gain access to the machine
using illegitimate means.

This attack vector exists for all of our infrastructure, to various
extents and is mitigated by trust in our upstream providers, our
monitoring infrastructure, timely security updates, and full disk
encryption.
   
## Layer 1: TPA sysadmins

TPA system administrators have access to all machines managed by
TPA.

## Layer 2: filesystem permissions

TPA admins can restrict access to repositories in an emergency by making
them unreadable. This was done on the `svn-internal` repository five
months ago, in ticket #15949 by anarcat.

## Layer 3: SVN admins

SVN service admins have access to the `svn-access-policy` repository
which defines the other two access layers below. That repository is
protected, like other repositories, by HTTPS authentication and SVN
access controls.

Unfortunately, the svn-access-policy repository uses a shared HTTPS
authentication database which means more users may have access to the
repository and only SVN access control restrict which ones of those
have actual access to the policy.

## Layer 4: HTTPS authentication

The remaining SVN repositories can be protected by HTTPS-level
authentication, defined by the Apache webserver configuration. For
"corp-svn", that configuration file is
`private/svn-access-passwords.corp`.

The SVN repositories currently accessible include:

 * /vidalia (public)
 * /svn-access-policy (see layer 3)
 * /corp (see above)
 * /internal (deactivated in layer 2)

## Layer 5: SVN access control

The last layer of defense is the SVN "group" level access control,
defined in the `svn-access-policy.corp` configuration file. In
practice, however, I believe that only Layer 4 HTTPS access controls
work for the corp repository.

Note that other repositories define other access controls, in particular
the `svn-access-policy` repository has its own configuration file, as
explained in layer 3.

## Notes

The the above list, SVN configuration files are located in
`/srv/svn.torproject.org/svn-access/wc/`, the "working copy" of the
`svn-access` repository.

This document is a redacted version of a fuller audit provided
internally in march 2020.
