Migrated Tickets per Gitlab Project
-----------------------------------

tpo/applications/tor-browser-bundle-testsuite: 150
tpo/applications/tor-launcher: 191
tpo/applications/rbm: 36
tpo/anti-censorship/bridgedb: 399
tpo/anti-censorship/censorship-analysis: 50
tpo/anti-censorship/pluggable-transports/obfs4: 19
tpo/anti-censorship/pluggable-transports/trac: 166
tpo/anti-censorship/pluggable-transports/snowflake: 301
tpo/anti-censorship/pluggable-transports/meek: 89
tpo/anti-censorship/trac: 34
tpo/anti-censorship/wolpertinger: 4
tpo/community/outreach: 53
tpo/community/relays-trac: 53
tpo/community/support: 108
tpo/community/trac: 40
tpo/community/meta-trac: 7
tpo/community/l10n: 155
tpo/core/tor: 9992
tpo/core/chutney: 293
tpo/core/dirauth: 51
tpo/core/fallback-scripts: 109
tpo/core/rpm-package: 33
tpo/core/tordnsel: 20
tpo/core/torsocks: 143
tpo/core/trunnel: 8
tpo/core/pytorctl: 6
tpo/network-health/sbws: 302
tpo/network-health/torflow: 176
tpo/network-health/doctor: 30
tpo/applications/https-everywhere-eff: 1223
tpo/applications/https-everywhere-chrome: 137
tpo/applications/https-everywhere: 12
tpo/tpa/schleuder: 14
tpo/tpa/services: 2155
tpo/metrics/trac: 64
tpo/metrics/utilities: 22
tpo/metrics/compass: 59
tpo/metrics/globe: 42
tpo/metrics/weather: 75
tpo/metrics/analysis: 145
tpo/metrics/cloud: 40
tpo/metrics/collector: 290
tpo/metrics/consensus-health: 65
tpo/metrics/exit-scanner: 20
tpo/metrics/exonerator: 63
tpo/metrics/ideas: 28
tpo/metrics/library: 153
tpo/metrics/onionoo: 346
tpo/metrics/onionperf: 86
tpo/metrics/relay-search: 320
tpo/metrics/statistics: 45
tpo/metrics/website: 331
tpo/ux/trac: 18
tpo/web/research: 15
tpo/web/tor-check: 88
tpo/web/manual-trac: 57
tpo/web/trac: 1311
tpo/web/styleguide-trac: 21
tpo/web/blog-trac: 140
tpo/web/mirrors: 19
tpo/web/support-trac: 50

Handled Components
------------------
  - DONE: 'Applications/Quality Assurance and Testing' -> 'tpo/applications/tor-browser-bundle-testsuite' (149 tickets: 69 open vs. 80 closed)
  - DONE: 'Applications/Tor Check' -> 'tpo/web/tor-check' (87 tickets: 7 open vs. 80 closed)
  - DONE: 'Applications/Tor Launcher' -> 'tpo/applications/tor-launcher' (190 tickets: 46 open vs. 144 closed)
  - DONE: 'Applications/rbm' -> 'tpo/applications/rbm' (35 tickets: 12 open vs. 23 closed)
  - DONE: 'Archived/DocTor' -> 'tpo/network-health/doctor' (29 tickets: 0 open vs. 29 closed)
  - DONE: 'Circumvention' -> 'tpo/anti-censorship/trac' (33 tickets: 23 open vs. 10 closed)
  - DONE: 'Circumvention/BridgeDB' -> 'tpo/anti-censorship/bridgedb' (398 tickets: 53 open vs. 345 closed)
  - DONE: 'Circumvention/Censorship analysis' -> 'tpo/anti-censorship/censorship-analysis' (49 tickets: 16 open vs. 33 closed)
  - DONE: 'Circumvention/Obfs4' -> 'tpo/anti-censorship/pluggable-transports/obfs4' (18 tickets: 12 open vs. 6 closed)
  - DONE: 'Circumvention/Pluggable transport' -> 'tpo/anti-censorship/pluggable-transports/trac' (165 tickets: 51 open vs. 114 closed)
  - DONE: 'Circumvention/Snowflake' -> 'tpo/anti-censorship/pluggable-transports/snowflake' (300 tickets: 70 open vs. 230 closed)
  - DONE: 'Circumvention/Wolpertinger' -> 'tpo/anti-censorship/wolpertinger' (3 tickets: 3 open vs. 0 closed)
  - DONE: 'Circumvention/meek' -> 'tpo/anti-censorship/pluggable-transports/meek' (88 tickets: 14 open vs. 74 closed)
  - DONE: 'Community' -> 'tpo/community/trac' (39 tickets: 17 open vs. 22 closed)
  - DONE: 'Community/Mirrors' -> 'tpo/web/mirrors' (18 tickets: 14 open vs. 4 closed)
  - DONE: 'Community/Outreach' -> 'tpo/community/outreach' (52 tickets: 16 open vs. 36 closed)
  - DONE: 'Community/Relays' -> 'tpo/community/relays-trac' (52 tickets: 28 open vs. 24 closed)
  - DONE: 'Community/Tor Browser Manual' -> 'tpo/web/manual-trac' (56 tickets: 15 open vs. 41 closed)
  - DONE: 'Community/Tor Support' -> 'tpo/community/support' (107 tickets: 18 open vs. 89 closed)
  - DONE: 'Community/Training' -> 'tpo/community/meta-trac' (6 tickets: 2 open vs. 4 closed)
  - DONE: 'Community/Translations' -> 'tpo/community/l10n' (154 tickets: 16 open vs. 138 closed)
  - DONE: 'Core Tor' -> 'tpo/core/tor' (58 tickets: 20 open vs. 38 closed)
  - DONE: 'Core Tor/Chutney' -> 'tpo/core/chutney' (292 tickets: 102 open vs. 190 closed)
  - DONE: 'Core Tor/DirAuth' -> 'tpo/core/dirauth' (50 tickets: 6 open vs. 44 closed)
  - DONE: 'Core Tor/Fallback Scripts' -> 'tpo/core/fallback-scripts' (108 tickets: 21 open vs. 87 closed)
  - DONE: 'Core Tor/RPM packaging' -> 'tpo/core/rpm-package' (32 tickets: 1 open vs. 31 closed)
  - DONE: 'Core Tor/Tor' -> 'tpo/core/tor' (9933 tickets: 2070 open vs. 7863 closed)
  - DONE: 'Core Tor/TorDNSEL' -> 'tpo/core/tordnsel' (19 tickets: 3 open vs. 16 closed)
  - DONE: 'Core Tor/Torflow' -> 'tpo/network-health/torflow' (175 tickets: 49 open vs. 126 closed)
  - DONE: 'Core Tor/Torsocks' -> 'tpo/core/torsocks' (142 tickets: 42 open vs. 100 closed)
  - DONE: 'Core Tor/Trunnel' -> 'tpo/core/trunnel' (7 tickets: 3 open vs. 4 closed)
  - DONE: 'Core Tor/pytorctl' -> 'tpo/core/pytorctl' (5 tickets: 4 open vs. 1 closed)
  - DONE: 'Core Tor/sbws' -> 'tpo/network-health/sbws' (301 tickets: 123 open vs. 178 closed)
  - DONE: 'HTTPS Everywhere' -> 'tpo/applications/https-everywhere' (11 tickets: 4 open vs. 7 closed)
  - DONE: 'HTTPS Everywhere/EFF-HTTPS Everywhere' -> 'tpo/applications/https-everywhere-eff' (1222 tickets: 253 open vs. 969 closed)
  - DONE: 'HTTPS Everywhere/HTTPS Everywhere: Chrome' -> 'tpo/applications/https-everywhere-chrome' (136 tickets: 52 open vs. 84 closed)
  - DONE: 'Internal Services/Schleuder' -> 'tpo/tpa/schleuder' (13 tickets: 4 open vs. 9 closed)
  - DONE: 'Internal Services' -> 'tpo/tpa/services' (14 tickets: 3 open vs. 11 closed)
  - DONE: 'Internal Services/Service - cache' -> 'tpo/tpa/services' (5 tickets: 2 open vs. 3 closed)
  - DONE: 'Internal Services/Service - deb.tpo' -> 'tpo/tpa/services' (28 tickets: 3 open vs. 25 closed)
  - DONE: 'Internal Services/Service - dist' -> 'tpo/tpa/services' (23 tickets: 13 open vs. 10 closed)
  - DONE: 'Internal Services/Service - git' -> 'tpo/tpa/services' (358 tickets: 10 open vs. 348 closed)
  - DONE: 'Internal Services/Service - github tpo' -> 'tpo/tpa/services' (5 tickets: 0 open vs. 5 closed)
  - DONE: 'Internal Services/Service - jabber' -> 'tpo/tpa/services' (2 tickets: 0 open vs. 2 closed)
  - DONE: 'Internal Services/Service - jenkins' -> 'tpo/tpa/services' (49 tickets: 8 open vs. 41 closed)
  - DONE: 'Internal Services/Service - lists' -> 'tpo/tpa/services' (133 tickets: 7 open vs. 126 closed)
  - DONE: 'Internal Services/Service - nextcloud' -> 'tpo/tpa/services' (22 tickets: 3 open vs. 19 closed)
  - DONE: 'Internal Services/Service - rt' -> 'tpo/tpa/services' (1 tickets: 0 open vs. 1 closed)
  - DONE: 'Internal Services/Service - sandstorm' -> 'tpo/tpa/services' (4 tickets: 0 open vs. 4 closed)
  - DONE: 'Internal Services/Service - trac' -> 'tpo/tpa/services' (362 tickets: 64 open vs. 298 closed)
  - DONE: 'Internal Services/Services Admin Team' -> 'tpo/tpa/services' (48 tickets: 18 open vs. 30 closed)
  - DONE: 'Internal Services/Tor Sysadmin Team' -> 'tpo/tpa/services' (1100 tickets: 110 open vs. 990 closed)
  - DONE: 'Metrics' -> 'tpo/metrics/trac' (63 tickets: 21 open vs. 42 closed)
  - DONE: 'Metrics Utilities' -> 'tpo/metrics/utilities' (21 tickets: 0 open vs. 21 closed)
  - DONE: 'Metrics/Analysis' -> 'tpo/metrics/analysis' (144 tickets: 23 open vs. 121 closed)
  - DONE: 'Metrics/Cloud' -> 'tpo/metrics/cloud' (39 tickets: 24 open vs. 15 closed)
  - DONE: 'Metrics/CollecTor' -> 'tpo/metrics/collector' (289 tickets: 38 open vs. 251 closed)
  - DONE: 'Metrics/Compass' -> 'tpo/metrics/compass' (58 tickets: 0 open vs. 58 closed)
  - DONE: 'Metrics/Consensus Health' -> 'tpo/metrics/consensus-health' (64 tickets: 18 open vs. 46 closed)
  - DONE: 'Metrics/Exit Scanner' -> 'tpo/metrics/exit-scanner' (19 tickets: 5 open vs. 14 closed)
  - DONE: 'Metrics/ExoneraTor' -> 'tpo/metrics/exonerator' (62 tickets: 7 open vs. 55 closed)
  - DONE: 'Metrics/Globe' -> 'tpo/metrics/globe' (41 tickets: 0 open vs. 41 closed)
  - DONE: 'Metrics/Ideas' -> 'tpo/metrics/ideas' (27 tickets: 25 open vs. 2 closed)
  - DONE: 'Metrics/Library' -> 'tpo/metrics/library' (152 tickets: 20 open vs. 132 closed)
  - DONE: 'Metrics/Onionoo' -> 'tpo/metrics/onionoo' (345 tickets: 42 open vs. 303 closed)
  - DONE: 'Metrics/Onionperf' -> 'tpo/metrics/onionperf' (85 tickets: 29 open vs. 56 closed)
  - DONE: 'Metrics/Relay Search' -> 'tpo/metrics/relay-search' (319 tickets: 23 open vs. 296 closed)
  - DONE: 'Metrics/Statistics' -> 'tpo/metrics/statistics' (44 tickets: 8 open vs. 36 closed)
  - DONE: 'Metrics/Tor Weather' -> 'tpo/metrics/weather' (74 tickets: 0 open vs. 74 closed)
  - DONE: 'Metrics/Website' -> 'tpo/metrics/website' (330 tickets: 41 open vs. 289 closed)
  - DONE: 'UX/Research' -> 'tpo/ux/trac' (17 tickets: 2 open vs. 15 closed)
  - DONE: 'Webpages' -> 'tpo/web/trac' (54 tickets: 20 open vs. 34 closed)
  - DONE: 'Webpages/Blog' -> 'tpo/web/blog-trac' (139 tickets: 30 open vs. 109 closed)
  - DONE: 'Webpages/Research' -> 'tpo/web/research' (14 tickets: 10 open vs. 4 closed)
  - DONE: 'Webpages/Styleguide' -> 'tpo/web/styleguide-trac' (20 tickets: 8 open vs. 12 closed)
  - DONE: 'Webpages/Support' -> 'tpo/web/support-trac' (49 tickets: 10 open vs. 39 closed)
  - DONE: 'Webpages/Website' -> 'tpo/web/trac' (1242 tickets: 54 open vs. 1188 closed)
  - DONE: 'Webpages/Webtools' -> 'tpo/web/trac' (14 tickets: 5 open vs. 9 closed)

Unhandled Components
--------------------
  - '- Select a component' -> 'legacy/trac' (291 tickets: 3 open vs. 288 closed)
  - 'Applications' -> 'legacy/trac' (46 tickets: 25 open vs. 21 closed)
  - 'Applications/GetTor' -> 'legacy/trac' (180 tickets: 40 open vs. 140 closed)
  - 'Applications/Orbot' -> 'legacy/trac' (284 tickets: 139 open vs. 145 closed)
  - 'Applications/Tor Browser' -> 'legacy/trac' (5879 tickets: 1894 open vs. 3985 closed)
  - 'Applications/Tor bundles/installation' -> 'legacy/trac' (796 tickets: 0 open vs. 796 closed)
  - 'Applications/TorBirdy' -> 'legacy/trac' (193 tickets: 29 open vs. 164 closed)
  - 'Applications/Torbutton' -> 'legacy/trac' (503 tickets: 0 open vs. 503 closed)
  - 'Archived/Agile' -> 'legacy/trac' (28 tickets: 0 open vs. 28 closed)
  - 'Archived/Development Progress' -> 'legacy/trac' (24 tickets: 0 open vs. 24 closed)
  - 'Archived/FTE' -> 'legacy/trac' (10 tickets: 2 open vs. 8 closed)
  - 'Archived/Flashproxy' -> 'legacy/trac' (132 tickets: 0 open vs. 132 closed)
  - 'Archived/Leekspin' -> 'legacy/trac' (2 tickets: 1 open vs. 1 closed)
  - 'Archived/Metrics Bot' -> 'legacy/trac' (35 tickets: 16 open vs. 19 closed)
  - 'Archived/Nyx' -> 'legacy/trac' (210 tickets: 1 open vs. 209 closed)
  - 'Archived/Obfsproxy' -> 'legacy/trac' (216 tickets: 21 open vs. 195 closed)
  - 'Archived/Ooni' -> 'legacy/trac' (603 tickets: 0 open vs. 603 closed)
  - 'Archived/Ponies' -> 'legacy/trac' (27 tickets: 0 open vs. 27 closed)
  - 'Archived/Stegotorus' -> 'legacy/trac' (17 tickets: 0 open vs. 17 closed)
  - 'Archived/Stem' -> 'legacy/trac' (526 tickets: 0 open vs. 526 closed)
  - 'Archived/Thandy' -> 'legacy/trac' (17 tickets: 0 open vs. 17 closed)
  - 'Archived/Tor Browser Sandbox' -> 'legacy/trac' (107 tickets: 0 open vs. 107 closed)
  - 'Archived/Tor Cloud' -> 'legacy/trac' (82 tickets: 0 open vs. 82 closed)
  - 'Archived/Tor Mail' -> 'legacy/trac' (4 tickets: 2 open vs. 2 closed)
  - 'Archived/Tor Messenger' -> 'legacy/trac' (219 tickets: 0 open vs. 219 closed)
  - 'Archived/Tor VM' -> 'legacy/trac' (7 tickets: 0 open vs. 7 closed)
  - 'Archived/Torouter' -> 'legacy/trac' (54 tickets: 0 open vs. 54 closed)
  - 'Archived/Torperf' -> 'legacy/trac' (35 tickets: 0 open vs. 35 closed)
  - 'Archived/Vidalia' -> 'legacy/trac' (410 tickets: 0 open vs. 410 closed)
  - 'Archived/general' -> 'legacy/trac' (23 tickets: 0 open vs. 23 closed)
  - 'Archived/obfsclient' -> 'legacy/trac' (1 tickets: 0 open vs. 1 closed)
  - 'Archived/operations' -> 'legacy/trac' (46 tickets: 0 open vs. 46 closed)
  - 'Archived/ttdnsd' -> 'legacy/trac' (3 tickets: 0 open vs. 3 closed)
  - 'Company' -> 'legacy/trac' (192 tickets: 3 open vs. 189 closed)
  - 'Firefox Patch Issues' -> 'legacy/trac' (198 tickets: 0 open vs. 198 closed)
  - 'Hudson' -> 'legacy/trac' (10 tickets: 0 open vs. 10 closed)
  - 'Mixminion-Client' -> 'legacy/trac' (26 tickets: 0 open vs. 26 closed)
  - 'Mixminion-Other' -> 'legacy/trac' (14 tickets: 0 open vs. 14 closed)
  - 'Mixminion-Server' -> 'legacy/trac' (22 tickets: 0 open vs. 22 closed)
  - 'Polipo' -> 'legacy/trac' (38 tickets: 0 open vs. 38 closed)
  - 'Tor - Tor Control Panel' -> 'legacy/trac' (21 tickets: 0 open vs. 21 closed)
  - 'Tor-Backend / Core' -> 'legacy/trac' (1 tickets: 0 open vs. 1 closed)
  - 'Tor-Client' -> 'legacy/trac' (1 tickets: 0 open vs. 1 closed)
  - 'Tor-Other' -> 'legacy/trac' (6 tickets: 0 open vs. 6 closed)
  - 'Tor-Server' -> 'legacy/trac' (2 tickets: 0 open vs. 2 closed)
  - 'TorBrowserButton' -> 'legacy/trac' (266 tickets: 2 open vs. 264 closed)
  - 'TorStatus' -> 'legacy/trac' (24 tickets: 0 open vs. 24 closed)
  - 'Torbutton-Tor client' -> 'legacy/trac' (10 tickets: 0 open vs. 10 closed)
  - 'Torbutton-Torbutton' -> 'legacy/trac' (2 tickets: 0 open vs. 2 closed)
  - 'Torctl' -> 'legacy/trac' (34 tickets: 0 open vs. 34 closed)
  - 'Torify' -> 'legacy/trac' (9 tickets: 0 open vs. 9 closed)
  - 'UX' -> 'legacy/trac' (9 tickets: 6 open vs. 3 closed)
  - 'User Experience/Tor Support' -> 'legacy/trac' (77 tickets: 0 open vs. 77 closed)
  - 'blockfinder' -> 'legacy/trac' (1 tickets: 0 open vs. 1 closed)
  - 'pyobfsproxy' -> 'legacy/trac' (5 tickets: 0 open vs. 5 closed)
  - 'pyonionoo' -> 'legacy/trac' (12 tickets: 0 open vs. 12 closed)

Stats
-----
  - Tickets:         32401
  - Tickets moving:  20411 (62.99496929107127%)
  - Tickets staying: 11990 (37.005030708928736%)
