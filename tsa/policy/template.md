[[!meta title="TPA-RFC-X: RFC template"]]

Summary: this template describes the basic field a TPA-RFC policy
should have. Refer to [[tpa-rfc-1-policy]] for the actual policy requirements.

# Background

# Proposal

## Scope

## ...

# Examples

Examples:

 * ...

Counter examples:

 * ...

# Deadline


# Status

This proposal is currently in the `draft` state.

# References

See [[tpa-rfc-1-policy]].
