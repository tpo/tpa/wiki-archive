# onionperf-ops

[[!toc levels=3]]

## Overview

The various moving parts of this project were due to converge in the coming
months and so it's difficult to document this in the same way as other more
stable projects. This documents the parts, and their last known state, but
is more of a snapshot than a source of truth. It does not reflect what the
Ansible playbook would have deployed had we done the upgrades, that would look
very different.

## Deployed Instances

We currently have four deployed OnionPerf instances:

* op-ab - Scotland (University of Aberdeen)
* op-hk - Hong Kong (OTF Cloud)
* op-nl - Netherlands (OTF Cloud)
* op-us - United States (OTF Cloud)

The first of these may be shut down very soon.

The instances on the OTF cloud do not have a fixed end-date. The virtual
machines are managed via the eclips.is
[portal](https://portal.eclips.is/portal/). The sysadmin team have the password
for this. None of these machines are managed by the sysadmin team, the metrics
team is responsible for keeping them up to date, and secure enough to be on the
public Internet.

Login to the instances via SSH is using the `cloud` user, which has SSH keys for
the admins of the service. The cloud user can sudo to any user using the
sudo password that is specific to the host.

On each host, a `tmux` session runs as the `cloud` user. This means that sudo
is not generally needed for routine activities.

The command to start looks like:

```
/usr/bin/python /usr/local/bin/onionperf measure --tor=/home/cloud/tor/src/or/tor --tgen=/home/cloud/shadow/src/plugin/shadow-plugin-tgen/build/tgen --tgen-listen-port 8080 --tgen-connect-port 80 --twistd=/usr/local/bin/twistd
```

## Changes to OnionPerf since last deployment

The OnionPerf codebase has undergone significant development since the op-*
instances were last updated. Some of these changes will require changes to the
way that OnionPerf is deployed.

* Reprocessing mode allows overwriting the source field in the resulting
  *.tpf and *.json files to an arbitrary string
* Onionperf now includes the tgen error code in the *.tpf and *.json
  outputs for transfers that do not complete.
* Onionperf now supports reprocessing past data with new analysis fields
  provided all the past logs are provided. This supports filtering by date.
* Onionperf has persisting onion addresses, which is useful for
  monitoring. Addresses and private keys are stored in a new directory,
  onionperf-private. All the logs and results are stored in a new
  directory, onionperf-data.
* Onionperf no longer outputs a DATAPERC field in the *.tpf file where the
  transfer did not complete due to an error
* Onionperf no longer outputs a payload_progress field in the *.json file
  where the transfer did not complete due to an error
* Onionperf now supports additional client and server tor configuration,
  which can be added on the command line as lines or as a file, or through
  an environment variable. Onionperf can now measure bridges, guards etc
* Onionperf supports  a ‘oneshot’ mode, where instead of running long-term
  measurements, I performs one 5M measurement and then shuts down
* Onionperf now has a test framework and unit tests for several functions
  in the codebase.
* Built-in twistd webserver used for sharing analysis results was removed,
  so that the data can be served by an external webserver (e.g. Apache)

## Deploying an OnionPerf

Follow instructions at https://2019.www.torproject.org/docs/debian.html.en to
install tor package.

Add Debian backports repository following instructions at
https://backports.debian.org/Instructions/#index2h2

Don't start tor at startup:

```
systemctl stop tor.service
systemctl mask tor.service
```

(mask prevents it from ever being started as a service)

Install python-stem from backports:

```
apt install -t buster-backports python-stem
```

Install OnionPerf dependencies with apt:

```
apt install cmake git libglib2.0-dev libigraph0-dev make python-dev python-ipaddress python-lxml python-matplotlib python-netifaces python-networkx python-nose python-numpy python-scipy
```

In the home directory for cloud (/home/cloud):

```
git clone https://git.torproject.org/onionperf.git
git clone https://github.com/shadow/tgen.git # use commit 10d32bb0 here, not master!
mkdir -p /home/cloud/tgen/build
cd /home/cloud/tgen/build
cmake ..
make
sudo ln -s /home/cloud/tgen/build/tgen /usr/bin/tgen
cd /home/cloud/onionperf
sudo python setup.py install
```

In tmux:

```
cd /home/cloud && /usr/local/bin/onionperf measure --tgen-listen-port 8080 --tgen-connect-port 443
```

-- need to fix XML-generating ~/onionperf/onionperf/model.py

For the web server:

```
apt install apache2
```

Replace /etc/apache2/sites-enabled/000-default.conf with:

```
<VirtualHost *:80>
 ServerName op-karsten.safemetrics.org
 DocumentRoot /home/cloud/onionperf-data
 <Directory /home/cloud/onionperf-data>
 Options Indexes
 Require all granted
 </Directory>
</VirtualHost>
```

Run:

```
sudo apt install certbot python-certbot-apache
sudo certbot --apache # choose 2, redirect
```

Edit /etc/apache2/sites-enabled/000-default-le-ssl.conf, change port to 8443.
Edit /etc/apache2/ports.conf to move Apache2 over to 8443.

For the firewall:

```
sudo iptables -A INPUT -p tcp -m tcp --dport 8443 -j ACCEPT
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 443 -j REDIRECT --to-ports 8080
sudo iptables-save | sudo tee /etc/iptables/rules.v4
```

If you need to restore the firewall for some reason:

```
sudo iptables-restore < /etc/iptables/rules.v4
```

